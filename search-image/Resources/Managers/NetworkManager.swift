//
//  NetworkManager.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import Foundation

class NatworkManager {
    
    func searchImageWith(text: String, callBack: @escaping (ResultModel?) -> Void) {
        let headers = [
            "x-rapidapi-host": "contextualwebsearch-websearch-v1.p.rapidapi.com",
            "x-rapidapi-key": "2ef472ae90msh7cdd7611d7598f6p1c4da3jsn2aeb245199a4"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI?autoCorrect=false&pageNumber=1&pageSize=1&q=\(text)&safeSearch=false")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error?.localizedDescription ?? "=== ERROR unrecognized ===")
                callBack(nil)
            } else {
                guard let data = data else { return }
                guard let resultModel = try? JSONDecoder().decode(ResultModel.self, from: data) else { return }
                callBack(resultModel)
            }
        })
        dataTask.resume()
    }
    
}
