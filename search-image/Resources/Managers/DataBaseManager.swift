//
//  DataBaseManager.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import RealmSwift

class DataBaseManager {
    
    var currentDate: String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let currentDate = formatter.string(from: date)
        return currentDate
    }
    
    func addNewObjectWith(value: Value, requestText: String) {
        let realm = try! Realm()
        let newObject = RealmModel()
        newObject.imageUrl = value.url ?? ""
        newObject.requestText = requestText
        newObject.createDate = currentDate
        try? realm.write {
            realm.add(newObject)
        }
    }
    
    func getAllObjects() -> [RealmModel] {
        let realm = try? Realm()
        guard let allObjects = realm?.objects(RealmModel.self) else { return [RealmModel()] }
        return Array(allObjects).sorted(by: { (firstObject, secondObject) -> Bool in
            return firstObject.createDate > secondObject.createDate
        })
    }
}
