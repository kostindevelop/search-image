//
//  BaseViewController.swift
//  search-image
//
//  Created by Konstantin Igorevich on 16.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
    }
    
    func showErrorAlertWith(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let actionOkey = UIAlertAction(title: "Ok", style: .cancel) { _ in}
        alert.addAction(actionOkey)
        present(alert, animated: true, completion: nil)
    }
}
