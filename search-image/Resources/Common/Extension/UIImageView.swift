//
//  UIImage.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func downloadImage(from urlString: String) {
        
        var imageUrlString: String?
        if urlString != "" {
        imageUrlString = urlString
        
        let url = URL(string: urlString)
        
        image = nil
        
            if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
                DispatchQueue.main.async {
                    self.image = cachedImage
                    return
                }
            }
            let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        if let downloadedImage = UIImage(data: data!) {
                            
                            if imageUrlString == urlString {
                                self.image = downloadedImage
                            }
                            
                            imageCache.setObject(downloadedImage, forKey: url as AnyObject)
                        }
                    }
                }
            }
            task.resume()
        }
    }
}
