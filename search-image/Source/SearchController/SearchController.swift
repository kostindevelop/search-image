//
//  SearchController.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SearchController: BaseViewController, NVActivityIndicatorViewable {
    
    private let tableView = UITableView()
    
    private let networkManager = NatworkManager()
    private let dataBaseManager = DataBaseManager()
    
    private var allRequestHistory = [RealmModel]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configuredUI()
        setupNavigationBar()
        setupTableView()
        registeredCells()
        fetchLocal()
    }
    
    private func configuredUI() {
        navigationItem.title = "Search image"
        view.backgroundColor = .white
    }
    
    private func setupNavigationBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.frame
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
    }
    
    private func registeredCells() {
        tableView.register(ImageCell.self, forCellReuseIdentifier: "ImageCell")
    }
    
    private func fetchLocal() {
        allRequestHistory = dataBaseManager.getAllObjects()
    }
}


// MARK: - UISearchBarDelegate

extension SearchController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        startAnimating(type: .ballScaleMultiple, color: .systemTeal)
        let searchText = searchBar.text ?? ""
        networkManager.searchImageWith(text: searchText) { result in
            DispatchQueue.main.async {
                self.stopAnimating()
                guard let result = result else {
                    self.showErrorAlertWith(message: "I can not connect to the server!")
                    return
                }
                if let value = result.value?.first {
                    self.dataBaseManager.addNewObjectWith(value: value, requestText: searchText)
                    self.fetchLocal()
                } else {
                    self.showErrorAlertWith(message: "No images found for your request!")
                }
            }
        }
    }
}


// MARK: - UITableViewDataSource

extension SearchController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRequestHistory.isEmpty ? 0 : allRequestHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as? ImageCell else { return UITableViewCell() }
        let requestHistory = allRequestHistory[indexPath.row]
        cell.searchObject = requestHistory
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}


// MARK: - UITableViewDelegate

extension SearchController: UITableViewDelegate {}
