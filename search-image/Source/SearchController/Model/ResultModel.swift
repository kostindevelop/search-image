//
//  ResultModel.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//
//   let resultModel = try? JSONDecoder().decode(ResultModel.self, from: jsonData)

import Foundation

// MARK: - ResultModel
struct ResultModel: Codable {
    var type: String?
    var totalCount: Int?
    var value: [Value]?

    enum CodingKeys: String, CodingKey {
        case type = "_type"
        case totalCount, value
    }
}

// MARK: - Value
struct Value: Codable {
    var url: String?
    var height, width: Int?
    var thumbnail: String?
    var thumbnailHeight, thumbnailWidth: Int?
    var base64Encoding, name, title: String?
    var imageWebSearchURL: String?

    enum CodingKeys: String, CodingKey {
        case url, height, width, thumbnail, thumbnailHeight, thumbnailWidth, base64Encoding, name, title
        case imageWebSearchURL = "imageWebSearchUrl"
    }
}
