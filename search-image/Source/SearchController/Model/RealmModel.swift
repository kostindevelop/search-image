//
//  ImageModel.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import RealmSwift

class RealmModel: Object {
    @objc dynamic var requestText: String = ""
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var createDate: String = ""
}
