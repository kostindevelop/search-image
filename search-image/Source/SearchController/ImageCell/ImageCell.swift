//
//  ImageCell.swift
//  search-image
//
//  Created by Konstantin Igorevich on 15.03.2020.
//  Copyright © 2020 Konstantin Igorevich. All rights reserved.
//

import UIKit

class ImageCell : UITableViewCell {
    
    var searchObject : RealmModel? {
        didSet {
            searchTextLabel.text = "Search text: " + (searchObject?.requestText ?? "")
            searchResultImage.downloadImage(from: searchObject?.imageUrl ?? "")
            searchDateLabel.text = "Search time: " + (searchObject?.createDate ?? "")
        }
    }
    
    private let searchTextLabel : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        return label
    }()
    
    private let searchDateLabel : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .left
        return label
    }()
    
    private let searchResultImage : UIImageView = {
        let imgView = UIImageView(image: UIImage())
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(searchResultImage)
        addSubview(searchTextLabel)
        addSubview(searchDateLabel)
        
        searchResultImage.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 10, paddingBottom: 5, paddingRight: 0, width: 90, height: 0, enableInsets: false)
        searchTextLabel.anchor(top: topAnchor, left: searchResultImage.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
        searchDateLabel.anchor(top: searchTextLabel.bottomAnchor, left: searchResultImage.rightAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
